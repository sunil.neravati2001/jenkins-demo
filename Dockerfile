FROM openjdk:17
EXPOSE 8080
ADD target/jenkins-demo-0.0.1-SNAPSHOT.war jenkins-demo-0.0.1-SNAPSHOT.war
ENTRYPOINT ["java", "-jar", "/jenkins-demo-0.0.1-SNAPSHOT.war"]
